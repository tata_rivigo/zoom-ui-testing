exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['todo.js'],
  jasmineNodeOpts: {
	showColors: true,
	defaultTimeoutInterval: 100000,
	isVerbose: true
  },
  onPrepare: function() {
    browser.driver.manage().window().maximize();
  }
};
