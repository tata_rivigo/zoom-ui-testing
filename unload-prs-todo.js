describe('angularjs homepage todo list', function() {
  var Intercept = require('protractor-intercept');
  var intercept = new Intercept(browser);
    
  it('login', function() {
  	browser.waitForAngularEnabled(false);
    browser.get('http://login.stg.rivigo.com/sso/login?client_id=zoom-develop-v2-client');

    element(by.id('login-username')).sendKeys(browser.params.email);
    element(by.id('login-password')).sendKeys(browser.params.password);
    element(by.id('login')).click();

    browser.waitForAngularEnabled(true);
    browser.driver.sleep(7000);
    element(by.id('toggle-nav-label')).click();
  });

  it('open prq page', function(){
  	intercept.addListener();
    element(by.css('[ng-click="clickMenuItem(\'arrivalpending\')"]')).click();
  });

  it('search pickup', function(){
    element(by.id('sInput')).sendKeys(browser.params.cnote);
  });

  it('click assign/reassign', function(){
    element(by.css('[ng-click="markReached($event,row)"]')).click();
  });

  it('click assign/reassign', function(){
    element(by.css('[ng-click="dialogData.onConfirm()"]')).click();
  });
/*
  it('search pickup', function(){
    element(by.id('sInput')).sendKeys(browser.params.cnote);
  });
*/
  it('click assign/reassign', function(){
    element(by.css('[ng-click="assignOa($event,row)"][ng-show="showAssignOa(row) && newPRS"]')).click();
  });

  it('click assign/reassign', function(){
    element(by.css('[aria-label="Assign Self"]')).click();
    element(by.css('[ng-model="dockNo"]')).sendKeys(2);
  });

  it('submit', function(){
    element(by.css('[ng-click="callFunction(button.onClick)"][aria-hidden=false]')).click();
  });

  it('submit', function(){
    element(by.css('[ng-click="cancel()"]')).click();
  });

  it('scan',function(){
    element(by.css('[ng-click="scanPickup($event, row)"]')).click();
  });

  it('fill barcodes', function(){
    element(by.css('[ng-model="barcodeSequence"]')).sendKeys(browser.params.cnote);
    browser.actions().sendKeys(protractor.Key.ENTER).perform();
  });

  it('scan',function(){
    element(by.css('[ng-click="submitData()"]')).click();
  });

  it('close',function(){
    element(by.css('.ngdialog-close:before')).click();
  });

  it('logout', function() {
  	intercept.getRequests().then(function(reqs) {
      //make some assertions about what happened here 
      reqs.forEach(function(req){
      	console.log(req.responseURL);
      	console.log(req.statusText);
      	try{
      		var obj = JSON.parse(req.responseText);
      		console.log(obj.status);
      	}catch(e){
      	}
      });
      browser.driver.sleep(7000);
    });
    element(by.css('[ng-click="logout()"]')).click();
    browser.driver.sleep(7000);
  });
});
