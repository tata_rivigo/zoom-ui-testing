describe('angularjs homepage todo list', function() {
  var Intercept = require('protractor-intercept');
  var intercept = new Intercept(browser);
    
  it('login', function() {
  	browser.waitForAngularEnabled(false);
    browser.get('http://login.stg.rivigo.com/sso/login?client_id=zoom-develop-v2-client');


    element(by.id('login-username')).sendKeys('tata@rivigo.com');
    element(by.id('login-password')).sendKeys('Knockout3#');
    element(by.id('login')).click();

    browser.waitForAngularEnabled(true);
    browser.driver.sleep(7000);
    element(by.id('toggle-nav-label')).click();
  });

  it('first 3 pages', function(){
  	intercept.addListener();
    element(by.css('[ng-click="clickMenuItem(\'prqdashboard\')"]')).click();
    element(by.id('toggle-nav-label')).click();
    element(by.css('[ng-click="clickMenuItem(\'unassignedpickup\')"]')).click();
    element(by.id('toggle-nav-label')).click();
    element(by.css('[ng-click="clickMenuItem(\'trackpickup\')"]')).click();
    element(by.id('toggle-nav-label')).click();
  });

  it('second 3 pages', function() {
    element(by.css('[ng-click="clickMenuItem(\'arrivalpending\')"]')).click();
    element(by.id('toggle-nav-label')).click();
    element(by.css('[ng-click="clickMenuItem(\'incompletecnote\')"]')).click();
    element(by.id('toggle-nav-label')).click();
    element(by.css('[ng-click="clickMenuItem(\'pickupspending\')"]')).click();
    element(by.id('toggle-nav-label')).click();
  });

  it('trips', function() {
    element(by.css('[ng-click="clickMenuItem(\'trips\')"]')).click();
    element.all(by.repeater('tab in summaryTabs')).then(function(tabs) {
		tabs.forEach(function (tab) {

			var EC = protractor.ExpectedConditions;
			var iframe = element(by.css('[ng-if="loadingFlag"]'));
			browser.wait(EC.invisibilityOf(iframe), 5000);
		    element(by.css('.dashboardTable'));
		    tab.click();
		});
    });
    element(by.id('toggle-nav-label')).click();
  });

  it('deliveries', function() {
    element(by.css('[ng-click="clickMenuItem(\'deliveries\')"]')).click();
	var EC = protractor.ExpectedConditions;
	var tab=element(by.css('[ng-click="changeTab(1)"]'));
	var iframe = element(by.css('[ng-if="loadingFlag"]'));
	browser.wait(EC.invisibilityOf(iframe), 5000);
    element(by.css('.dashboardTable'));
    tab.click();
	var tab=element(by.css('[ng-click="changeTab(2)"]'));
	var iframe = element(by.css('[ng-if="loadingFlag"]'));
	browser.wait(EC.invisibilityOf(iframe), 5000);
    element(by.css('.dashboardTable'));
    tab.click();
    element(by.id('toggle-nav-label')).click();
  });

  it('loadView', function() {
  	browser.driver.manage().timeouts().setScriptTimeout(60000);
    element(by.css('[ng-click="clickMenuItem(\'loadview\')"]')).click();
	var EC = protractor.ExpectedConditions;
	var tab=element(by.css('[ng-click="setTab(1)"]'));
	var iframe = element(by.css('[ng-if="loadingFlag"]'));
	browser.wait(EC.invisibilityOf(iframe), 5000);
    element(by.css('.dashboardTable'));
    tab.click();
    element(by.id('toggle-nav-label')).click();
  });

  it('inbounddeps', function() {
    element(by.css('[ng-click="clickMenuItem(\'reportdeps\')"]')).click();
    element(by.id('toggle-nav-label')).click();
    element(by.css('[ng-click="clickMenuItem(\'inbounddeps\')"]')).click();
    element(by.id('toggle-nav-label')).click();
  });

  it('outbounddeps', function() {
    element(by.css('[ng-click="clickMenuItem(\'outbounddeps\')"]')).click();
    browser.driver.sleep(7000);
  });

/*
  it('sixth 3 pages', function() {
    element(by.id('toggle-nav-label')).click();
    element(by.css('[ng-click="clickMenuItem(\'clientmis\')"]')).click();
    element(by.id('toggle-nav-label')).click();
    element(by.css('[ng-click="clickMenuItem(\'clienttrackpickup\')"]')).click();
    element(by.id('toggle-nav-label')).click();
    element(by.css('[ng-click="clickMenuItem(\'clmpanel\')"]')).click();
    browser.driver.sleep(7000);
  });

  it('seventh 3 pages', function() {
    element(by.id('toggle-nav-label')).click();
    element(by.css('[ng-click="clickMenuItem(\'finsupport\')"]')).click();
    element(by.id('toggle-nav-label')).click();
    element(by.css('[ng-click="clickMenuItem(\'opcpanel\')"]')).click();
    element(by.id('toggle-nav-label')).click();
    element(by.css('[ng-click="clickMenuItem(\'techsupport\')"]')).click();
    browser.driver.sleep(7000);
  });
*/

  it('logout', function() {
  	intercept.getRequests().then(function(reqs) {
      //make some assertions about what happened here 
      reqs.forEach(function(req){
      	console.log(req.responseURL);
      	console.log(req.statusText);
      	try{
      		var obj = JSON.parse(req.responseText);
      		console.log(obj.status);
      	}catch(e){
      	}
      });
      browser.driver.sleep(7000);
    });
    element(by.css('[ng-click="logout()"]')).click();
    browser.driver.sleep(7000);
  });
});
